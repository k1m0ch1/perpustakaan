FROM python:3.6-alpine
MAINTAINER k1m0ch1

ENV user 'admin'
ENV email 'admin@admin.com'
ENV pass 'admin'

RUN apk update && apk add --no-cache  \
    git \
    ca-certificates \
    openssh-client \
    postgresql-dev \
    gcc \
    musl-dev

RUN mkdir -p /app
WORKDIR /app

COPY catalog/ /app/catalog/
COPY locallibrary/ /app/locallibrary/
COPY templates/ /app/templates/
COPY manage.py /app/manage.py
COPY requirements.txt /app/requirements.txt

RUN pip install -r requirements.txt

RUN python manage.py makemigrations
RUN python manage.py migrate
RUN python manage.py collectstatic
RUN python manage.py test
RUN echo "from django.contrib.auth.models import User; User.objects.create_superuser('${user}', '${email}', '${pass}')" | python manage.py shell

RUN find / -type f \( -name "*.pyx" -o -name "*.pyd"  -o -name "*.whl" \) -delete && \
    find /usr/local/lib/python3.6  -type f \( -name "*.c" -o -name "*.pxd"  -o -name "*.pyd" -o -name "__pycache__" \) -delete && \
    rm /app/requirements.txt

EXPOSE 8000
ENTRYPOINT ["python","manage.py", "runserver", "0.0.0.0:8000"]